#include<iostream>
#include<algorithm>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

int main(){
	char s1[200],s2[200];
	while(cin>>s1>>s2){
		int len=strlen(s1);
		int cnt1[26]={0},cnt2[26]={0};
		for(int i=0;i<len;i++){
			cnt1[s1[i]-'A']++;
			cnt2[s2[i]-'A']++;
		}
		std::sort(cnt1,cnt1+26);
		std::sort(cnt2,cnt2+26);
		int ok=1;
		for(int i=0;i<26;i++){
			if(cnt1[i]!=cnt2[i]){
				ok=0;
			}
		}
		if(ok){
			cout<<"YES"<<endl;
		} 
		else{
			cout<<"NO"<<endl;
		}
	}
	return 0;
}