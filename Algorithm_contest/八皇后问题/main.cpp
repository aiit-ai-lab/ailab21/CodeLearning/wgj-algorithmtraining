#include<iostream>

int C[50],n,tot=0;

void search(int cur){
	if(cur==n) tot++;
	for(int i=0;i<n;i++){
		int ok = 1;
		C[cur] = i;
		for(int j=0;j<cur;j++){
			if(C[cur]==C[j]||cur-C[cur]==j-C[j]||cur+C[cur]==j+C[j]){
				ok = 0;
				break;
			}
		}
		if(ok) search(cur+1);
	}
}

int main(){
	std::cin>>n;
	search(0);
	std::cout<<tot<<std::endl;
	return 0;
}