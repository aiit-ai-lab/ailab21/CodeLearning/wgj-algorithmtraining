#include<iostream>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

const int maxn=105;
char s[maxn];

int Less(const char* s,int p,int q){
	int n=strlen(s);
	for(int i=0;i<n;i++){
		if(s[(p+i)%n]!=s[(q+i)%n]){
			return s[(p+i)%n]<s[(q+i)%n];
		}
	}
	return 0;
}

int main(){
	int T;
	cin>>T;
	while(T--){
		cin>>s;
		int ans=0;
		int n=strlen(s);
		for(int i=1;i<n;i++){
			if(Less(s,i,ans)){
				ans=i;
			}
		}
		for(int i=0;i<n;i++){
			putchar(s[(ans+i)%n]);
		}
		cout<<endl;
	}
	return 0; 
}