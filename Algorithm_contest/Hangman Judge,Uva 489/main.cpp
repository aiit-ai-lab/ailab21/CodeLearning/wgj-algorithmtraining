#include<iostream>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

const int maxn=200;
int left,chance;
char s[maxn],s2[maxn];
int win,lose;

//猜一个字母 
void guess(char ch){
	int bad=1;
	for(int i=0;i<strlen(s);i++){
		if(ch==s[i]){
			left--;
			s[i]=' ';
			bad=0;
		}
	}
	if(bad){
		chance--;
	}
	if(!chance){
		lose=1;
	}
	if(!left){
		win=1;
	}
}

int main(){
	int rnd;
	while(cin>>rnd>>s>>s2&&rnd!=-1){
		cout<<"Round "<<rnd<<endl;
		win=0,lose=0;
		left=strlen(s);
		chance=7;
		for(int i=0;i<strlen(s2);i++){
			guess(s2[i]);
			if(win||lose){
				break;
			}
		}
		if(win){
			cout<<"You win."<<endl;
		}
		else if(lose){
			cout<<"You lose."<<endl;
		}
		else{
			cout<<"You chickened out."<<endl;
		}
	}
	return 0;
}