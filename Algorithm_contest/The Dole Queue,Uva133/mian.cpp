#include<iostream>
#include<iomanip>
using std::cin;
using std::cout;
using std::endl;

const int maxn=25;
int a[maxn];
int n,k,m;


int go(int p,int d,int t){
	while(t--){
		do{
			p=(p+d+n-1)%n+1;
		}
		while(a[p]==0);
	}
	return p;
}

int main(){
	while(cin>>n>>k>>m&&n){
		for(int i=1;i<=n;i++){
			a[i]=i;
		}
		int left=n;
		int p1 = n,p2 = 1;
		while(left){
			p1=go(p1,1,k);
			p2=go(p2,-1,m);
			if(p1==p2){
				cout<<std::setw(3)<<p1;
				left--;
			}
			else{
				cout<<std::setw(3)<<p1<<std::setw(3)<<p2;
				left-=2;
			}
			a[p1]=a[p2]=0;
			if(left){
				cout<<",";
			}
		}
		cout<<endl;
	}
	return 0;
}