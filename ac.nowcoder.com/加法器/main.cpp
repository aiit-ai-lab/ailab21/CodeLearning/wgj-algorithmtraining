#include<iostream>
#include<string>
#include<vector>
#include<algorithm>
using std::cin;
using std::cout;
using std::endl;
using std::vector;

vector<int> add(vector<int> &A,vector<int> &B){
	vector<int> res;
	int t=0;
	for(int i=0;i<std::max(A.size(),B.size());i++){
		if(i<A.size()){
			t+=A[i];
		}
		if(i<B.size()){
			t+=B[i];
		}
		res.push_back(t%10);
		t/=10;
	}
	if(t){
		res.push_back(1);
	}
	return res;
}

int main(){
	std::string a,b;
	while(cin>>a>>b){
		vector<int> A,B;
		for(int i = a.length()-1; i >= 0; i--){
			A.push_back(a[i]-'0');						
		}
		for(int i = b.length()-1; i >= 0; i--){
			B.push_back(b[i]-'0');
		}
		vector<int> C = add(A,B);
		for(int i=C.size()-1;i>=0;i--){
			cout<<C[i];
		}
		cout<<endl;
	}
	return 0;
}
 