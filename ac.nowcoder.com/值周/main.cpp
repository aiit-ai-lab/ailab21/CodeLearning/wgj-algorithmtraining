#include<iostream>
#include<cstring>
using namespace std;

const int maxn = 100000005;
int a[maxn];

int main(){
    memset(a,0,sizeof(a));
    int L, M;
    cin>>L>>M;
    int x, y;
    for(int i = 0; i < M; i++){
        cin>>x>>y; 
        a[x]++;
        a[y+1]--;
    }
    int sum = 0;
    int ans = 0;
    for(int i = 0; i <= L; i++){
        sum += a[i];
        if(sum == 0) ans++;
    }
    cout<<ans;
    return 0;
}