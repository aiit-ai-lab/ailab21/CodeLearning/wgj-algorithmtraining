#include<iostream>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

const int maxn=100005;
int s[maxn];

int main(){
    int L,M;
    cin>>L>>M;
    int a[105][2];
    for(int i=0;i<M;i++){
        cin>>a[i][0]>>a[i][1];
    }
    memset(s,1,sizeof(s));
    for(int j=0;j<M;j++){
        for(int i=a[j][0];i<=a[j][1];i++){
            s[i]=0;
        }
    }
    int ans=0;
    for(int i=0;i<=L;i++){
        if(s[i]){
            ans++;
        }
    }
    cout<<ans<<endl;
    return 0;
}