#include<iostream>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

const char* alpha="abcdefghijklmnopqrstuvwxyz";
const int maxn=206;
int a[206][206];

int main(){
    int n;
    while(cin>>n){
        int x=0,y=0,tot=0;
        memset(a,0,sizeof(a));
        
        while(tot<(2*n+1)*(2*n+1)){
            while(y+1<2*n+1&&!a[x][y+1]){
                a[x][++y]=++tot;
            }
            a[++x][y]=++tot;
            while(y-1>=0&&!a[x][y-1]){
                a[x][--y]=++tot;
            }
            a[++x][y]=++tot;
        }
        for(int x=0;x<2*n+1;x++){
            for(int y=0;y<2*n+1;y++){
                cout<<alpha[(0+a[x][y])%26]; 
            }
            cout<<endl;
        }
    }
    return 0;
}