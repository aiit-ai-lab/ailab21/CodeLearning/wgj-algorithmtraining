#include<iostream>
#include<cstring>
using std::cin;
using std::cout;
using std::endl;

int main(){
    int n,k,m;
    cin>>n>>k>>m;
    int a[105];
    memset(a,1,sizeof(a));
    int count=1;
    int x=0;
    int ans=0;
    for(int i=0;i<n;i++){
        if(ans==n-1){
            break;
        }
        while(i==k){
            if(ans==n-1){
                break;
            }
            if(!a[(i+x)%n]){
                x++;
                continue;
            }
            if(count==m){
                a[(i+x)%n]=0;
                count=1;
                x++;
                ans++;
                continue;
            }
            count++;
            x++;
        }
    }
    for(int i=0;i<n;i++){
        if(a[i]){
            cout<<i<<endl;
            break;
        }
    }
    return 0;
}