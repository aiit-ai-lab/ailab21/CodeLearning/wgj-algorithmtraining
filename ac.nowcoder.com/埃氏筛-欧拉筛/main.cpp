#include<iostream>
#include<cmath>
#include<cstring>
#include<algorithm>
using std::cin;
using std::cout;
using std::endl;

const int maxn = 1e6+5;
int a[maxn];

//埃氏筛 0为素数/1为非素数
void isPrime(){
	for(int i=2;i<=sqrt(maxn);i++){
		if(a[i]==0){
			for(int j=2*i;j<=maxn;j+=i){
				a[j]=1;
			}
		}
	}	
}

//欧拉筛
// void isPrime(){
// 	int count = 0;
// 	for(int i=2;i<=maxn;i++){
// 		if(a[i]==0){
// 			b[count++] = i;
// 		}
// 		for(int j=0;j<=count&&i*b[j]<=maxn;j++){
// 			a[i*b[j]]=1;
// 			if(i%b[j]==0){
// 				break;
// 			}
// 		}
// 	} 
// }

int main(){
	int T;
	cin>>T;
	memset(a,0,sizeof(a));
	isPrime();
	while(T--){
		int N,P;
		cin>>N>>P;
		long long ans=0;
		long long fac=1;
		for(int i=2;i<=std::min(N,P);i++){
			fac = fac*i%P;
			if(a[i]==0){
				ans = (ans+fac)%P;
			} 
		}
		cout<<ans<<endl;
	}
	return 0;
}